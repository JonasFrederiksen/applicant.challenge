using Xunit;
using FluentAssertions;
using Applicant.Challenge.Domain;
using Applicant.Challenge.Domain.Extensions;
using Applicant.Challenge.Core;


namespace Applicant.Challenge.Test.Domain
{
    public class BinaryTreeTest
    {
        [Fact]
        public void ResolvePath()
        {
            var root = new TreeNode(1, 215);
            var lvl1a = new TreeNode(2, 192, root);
            var lvl1b = new TreeNode(3, 124, root);
            var lvl2a = new TreeNode(4, 117, lvl1a);
            var lvl2b = new TreeNode(5, 269, lvl1a);
            var lvl2c = new TreeNode(6, 269, lvl1b);
            var lvl2d = new TreeNode(7, 442, lvl1b);

            var tree = new BinaryTree(root);
            tree.ResolvePaths(new OddEventPathResolver(tree));
            var path = tree.GetMaxWeight();
            path.TotalWeight.Should().Be(676);
        }
    }
}
