﻿using System;
using System.Collections.Generic;
using Applicant.Challenge.Core;
using Applicant.Challenge.SharedKernel;

namespace Applicant.Challenge.Domain
{
    public class BinaryTree : AggregateRoot
    {
        #region Properties
        public TreeNode Root { get; }
        public List<NodePath> Paths { get; }
        #endregion

        #region Cstr
        public BinaryTree(TreeNode node)
        {
            Root = node ?? throw new InvalidOperationException("Node must be initialized");
            Paths = new List<NodePath>();
        }
        #endregion

        #region
        /// <summary>
        /// The BinaryTree is the context of the Collection and the Iterator
        /// </summary>
        public void ResolvePaths(OddEventPathResolver resolver)
        {
            while (!resolver.IsDone)
            {
                resolver.Next();

                if (resolver.IsCurrentLeaf)
                    Paths.Add(resolver.Current.CreatePathToRoot());
            }
        }

        public OddEventPathResolver CreateResolver()
        {
            return new OddEventPathResolver(this);
        }
        #endregion
    }
}