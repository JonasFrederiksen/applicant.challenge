﻿using System.Linq;

namespace Applicant.Challenge.Domain.Extensions
{
    public static class BinaryTreeExtension
    {
        public static NodePath GetMaxWeight(this BinaryTree tree)
        {
            return tree.Paths.OrderByDescending(p => p.TotalWeight).First();
        }
    }
}