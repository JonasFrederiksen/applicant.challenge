﻿using System;
using System.Linq;

namespace Applicant.Challenge.Domain.Extensions
{
    public static class NodePathExtension
    {
        public static void Print(this NodePath path)
        {
            var ordered = path.Nodes.OrderBy(n => n.Id);
            foreach (var node in ordered)
                Console.WriteLine(node.Weight);
        }
    }
}