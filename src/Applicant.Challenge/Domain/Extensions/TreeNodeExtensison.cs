﻿using System.Linq;
using System.Collections.Generic;

namespace Applicant.Challenge.Domain.Extensions
{
    public static class TreeNodeExtensison
    {
        internal static bool IsEven(this TreeNode e)
        {
            return e.Weight % 2 == 0;
        }

        internal static IEnumerable<TreeNode> GetPossibleChildren(this IEnumerable<TreeNode> nodes, bool even)
        {
            return nodes.Where(e => e.IsEven() != even);
        }
    }
}