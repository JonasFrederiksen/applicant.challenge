﻿using System;
using System.Linq;
using System.Collections.Generic;
using Applicant.Challenge.SharedKernel;

namespace Applicant.Challenge.Domain
{
    public class NodePath : ValueObject
    {
        #region Properties
        public List<TreeNode> Nodes { get; }
        public int TotalWeight => Nodes.Sum(n => n.Weight);
        #endregion

        #region Cstr
        public NodePath(List<TreeNode> nodes)
        {
            if (nodes == null) throw new InvalidOperationException("Nodes must be initialized");
            if (nodes.Count <= 0) throw new InvalidOperationException("Empty path");

            Nodes = nodes;
        }
        #endregion
    }
}