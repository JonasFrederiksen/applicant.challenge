﻿using System;
using System.Linq;
using System.Collections.Generic;
using Applicant.Challenge.SharedKernel;

namespace Applicant.Challenge.Domain
{
    public class TreeNode : Entity
    {
        #region Properties
        public int Id { get; }
        public int Weight { get; }
        public TreeNode Parent { get; }
        public List<TreeNode> Children { get; }
        public bool IsLeaf => !Children.Any();
        #endregion

        #region Cstr
        public TreeNode(int id, int weight) 
            : this(id, weight, null)
        {

        }
        public TreeNode(int id, int weight, TreeNode parent)
        {
            Children = new List<TreeNode>();
            Id = id;
            Weight = weight;
            Parent = parent;

            if (parent != null)
                parent.AddChild(this);
        }
        #endregion

        #region
        public void AddChild(TreeNode node)
        {
            if (Children.Count >= 2) throw new InvalidOperationException("Too many childrin");

            Children.Add(node);
        }
        public NodePath CreatePathToRoot()
        {
            var path = new List<TreeNode>();
            var current = this;
            while (current != null)
            {
                path.Add(current);
                current = current.Parent;
            }

            return new NodePath(path);
        }
        #endregion

        #region Standard Override
        public override bool Equals(object obj)
        {
            var item = (TreeNode)obj;
            if (item == null) return false;

            return Id == item.Id;
        }

        public override string ToString()
        {
            return $"Id:{Id} - Weight:{Weight}";
        }
        #endregion
    }
}