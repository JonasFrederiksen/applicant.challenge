﻿using System;
using Applicant.Challenge.Core;
using Applicant.Challenge.Domain.Extensions;

namespace Applicant.Challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new BinaryTreeFactory();
            Console.WriteLine("Starting...");
            var tree = factory.Create();
            Console.WriteLine("Resolving path...");
            tree.ResolvePaths(tree.CreateResolver());
            Console.WriteLine("Find result...");
            var result = tree.GetMaxWeight();
            Console.WriteLine("Result");
            result.Print();
            Console.WriteLine("Complete...");
        }
    }
}