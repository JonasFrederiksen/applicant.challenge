﻿using System;
using System.Linq;
using System.Collections.Generic;
using Applicant.Challenge.Domain;
using Applicant.Challenge.Domain.Extensions;

namespace Applicant.Challenge.Core
{
    /// <summary>
    /// Domain Service
    /// </summary>
    public class OddEventPathResolver
    {
        #region Members
        private BinaryTree _binaryTree;
        private List<TreeNode> _visited;
        #endregion

        #region Properties
        private IEnumerable<TreeNode> CurrentChildren => Current.Children;
        private bool IsCurrentEven => Current.IsEven();
        private bool HasPossibleChildren => GetPossibleChildren().Any();

        public TreeNode First => _binaryTree.Root;
        public TreeNode Current { get; private set; }
        public bool IsDone => Current == First && !NotVisitedChildren().Any();
        public bool IsCurrentLeaf => Current.IsLeaf || !HasPossibleChildren;
        #endregion

        #region Cstr
        public OddEventPathResolver(BinaryTree binaryTree)
        {
            _visited = new List<TreeNode>();
            _binaryTree = binaryTree ?? throw new InvalidOperationException("BinaryTree must be initialized");
            Current = First;
        }
        #endregion

        #region 
        internal IEnumerable<TreeNode> GetPossibleChildren()
        {
            return CurrentChildren.GetPossibleChildren(IsCurrentEven);
        }
        internal IEnumerable<TreeNode> FilterVisitedNodes(IEnumerable<TreeNode> nodes)
        {
            return nodes.Where(e => !_visited.Contains(e));
        }
        internal IEnumerable<TreeNode> NotVisitedChildren()
        {
            var possible = GetPossibleChildren();
            return FilterVisitedNodes(possible);
        }
        
        public void Next()
        {
            var nodes = NotVisitedChildren();    

            if(nodes.Any())
            {
                var first = nodes.First();
                Current = first;
            }
            else
            {
                _visited.Add(Current);
                Current = Current.Parent;
            }
        }
        #endregion
    }
}