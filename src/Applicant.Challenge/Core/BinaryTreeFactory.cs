﻿using System;
using System.Linq;
using System.Collections.Generic;
using Applicant.Challenge.Data;
using Applicant.Challenge.Domain;


namespace Applicant.Challenge.Core
{
    /// <summary>
    /// Domain Service
    /// </summary>
    public class BinaryTreeFactory
    {
        private static IEnumerable<ChildNode> FindParentNodes(NodeData child, IEnumerable<ChildNode> nodes)
        {
            var parents = nodes.Where(n => n.DataNode.Level == (child.Level - 1));
            parents = parents.Where(n => n.DataNode.Index == child.Index || n.DataNode.Index == (child.Index - 1));
            return parents;
        }

        private static IEnumerable<NodeData> FindParents(NodeData child, NodeData[] nodes)
        {
            var parents = nodes.Where(n => n.Level == (n.Level - 1)).Where(n => n.Index == child.Index || n.Index == (child.Index - 1));
            return parents;
        }

        private TreeNode CreateTree(NodeData[] nodes)
        {
            if (nodes == null || !nodes.Any()) throw new InvalidOperationException("No data provided");

            var ordered = nodes.OrderBy(n => n.Level).ThenBy(n => n.Index);

            var map = new List<ChildNode>();

            var id = 0;
            foreach (var node in ordered)
            {
                var parents = FindParentNodes(node, map).ToArray();

                
                ChildNode child;

                if (!parents.Any())
                {
                    child = new ChildNode()
                    {
                        Id = ++id,
                        DataNode = node,
                        TreeNode = new TreeNode(id, node.Weight),
                    };
                    map.Add(child);
                }
                else
                {
                    foreach (var parent in parents)
                    {
                        child = new ChildNode()
                        {
                            Id = ++id,
                            DataNode = node,
                            TreeNode = new TreeNode(id, node.Weight, parent.TreeNode),
                        };
                        map.Add(child);
                    }
                }
            }

            var first = map.First();
            return first.TreeNode;
        }

        public BinaryTree Create()
        {
            var nodeData = new DataReader();
            var tree = CreateTree(nodeData.Nodes.ToArray());
            return new BinaryTree(tree);
        }

        private class ChildNode
        {
            public int Id { get; set; }
            public NodeData DataNode { get; set; }
            public TreeNode TreeNode { get; set; }

            public override bool Equals(object obj)
            {
                var item = (ChildNode)obj;
                if (item == null) return false;
                return Id == item.Id;
            }

            public override string ToString()
            {
                return $"{TreeNode}_{DataNode}";
            }
        }
    }
}