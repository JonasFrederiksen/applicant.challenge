﻿namespace Applicant.Challenge.Data
{
    public class NodeData
    {
        public string Id => $"{Level}_{Index}";
        public int Weight { get; set; }
        public int Level { get; set; }
        public int Index { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Weight}";
        }
    }
}
